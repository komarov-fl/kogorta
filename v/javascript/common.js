$(function () {
    
    var bodyOverflow;
    
    if ($().flexslider) {
        $('.home-slider .slider-container-inner').flexslider({
            animation: "slide",
            controlNav: false,
            customDirectionNav: $(".home-slider .slider-container-inner .custom-navigation a"),
            start: function(holder) {
                var totalSlides = $(holder).find('.slides li').not('.clone').length;
                $(holder).find('.total-slides').html(totalSlides);
            },
            after: function(holder) {
                var current = $(holder).find('.slides li.flex-active-slide').index();
                $(holder).find('.current-slide').html(current);
            }
        });
    }
    
    $('.request-price').on('click', function(){
        popupShow('request-price');
    });
    
    $('.popup > *, .popup .close').on('click', function(){
        $('.popup').fadeOut();
        $('body').css('overflow', bodyOverflow);
    });
    
    $('.popup > * > *').on('click', function(event){
        event.stopPropagation();
    });
    
    
    function popupShow(id) {
        $('#'+id).fadeIn().css('display', 'table');
        bodyOverflow = $('body').css('overflow');
        $('body').css('overflow', 'hidden');
    }
    
});