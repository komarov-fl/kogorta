<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'p374759.mysql.ihc.ru',
	'username' => 'p374759_kagorta',
	'password' => 'zuV94Wt8AQ',
	'database' => 'p374759_kagorta',
	'path' => ''
);

// Set the site locale
i18n::set_locale('ru_RU');

HtmlEditorConfig::get('cms')->setOption('content_css', project() . '/editor.css');

ShortcodeParser::get('default')->register('Gallery', array('Page', 'GaleryShortcode'));
ShortcodeParser::get('default')->register('FeedbackForm', array('Page', 'FeedbackFormShortcode'));
ShortcodeParser::get('default')->register('Map', array('Page', 'MapShortcode'));