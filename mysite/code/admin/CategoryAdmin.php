<?php

class CategoryAdmin extends ModelAdmin {
    
    private static $managed_models = array(
        'Category'
    );
    
    private static $url_segment = 'categories';

    private static $menu_title = 'Категории';
    
    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);

        $gridFieldName = 'Category';
        $gridField = $form->Fields()->fieldByName($gridFieldName);

        if ($gridField) {
            /* @var $config GridFieldConfig_RecordEditor */
            $config = $gridField->getConfig();             
            $config->removeComponentsByType('GridFieldDataColumns'); 
            
            $GridFieldDataColumns = new GridFieldDataColumns();            
            $GridFieldDataColumns->setDisplayFields(array(
                'CMSThumbnail' => 'Изображение',
                'Name'=>'Название',
                'Parent.NestedName' => 'Родительская категория'
            ));
            $config->addComponent($GridFieldDataColumns,'GridFieldEditButton');            
            
            
        }

        return $form;
    }
    
    public function getSearchContext() {
        $context = parent::getSearchContext();        
        $context->removeFieldByName('q[Description]');
        $context->addField(
            $ParentField = new DropdownField('q[ParentID]', 'Родительская категория', Category::get()->map('ID', 'NestedName'))
        );
        $ParentField->setHasEmptyDefault(true);
        
        return $context;
        
    }
    
    public function getList() {
        $list = parent::getList();

        $params = $this->getRequest()->requestVar('q'); // use this to access search parameters

        if( isset($params['ParentID']) && $params['ParentID']) {
            $list = $list->filter(array('ParentID'=>$params['ParentID'] ));
        }

        return $list;        
    }

}
