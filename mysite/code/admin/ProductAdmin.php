<?php

class ProductAdmin extends ModelAdmin {
    
    private static $managed_models = array(
        'Product'
    );
    

    private static $url_segment = 'products';

    private static $menu_title = 'Товары';
    
    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);

        $gridFieldName = 'Product';
        $gridField = $form->Fields()->fieldByName($gridFieldName);

        if ($gridField) {
            /* @var $config GridFieldConfig_RecordEditor */
            $config = $gridField->getConfig();             
            $config->removeComponentsByType('GridFieldDataColumns'); 
            
            $GridFieldDataColumns = new GridFieldDataColumns();            
            $GridFieldDataColumns->setDisplayFields(array(
                'CMSThumbnail' => 'Изображение',
                'Name'=>'Наименование',
                'Parent.NestedName' => 'Родительская категория'
            ));
            $config->addComponent($GridFieldDataColumns,'GridFieldEditButton');            
            
            
        }

        return $form;
    }
    
    public function getSearchContext() {
        $context = parent::getSearchContext();        
        $context->removeFieldByName('q[Description]');
        $context->addField(
            $ParentField = new DropdownField('q[ParentID]', 'Показываются в категориях', Category::get()->map('ID', 'NestedName'))
        );
        $ParentField->setHasEmptyDefault(true);
        
        return $context;
        
    }
    
    public function getList() {
        $list = parent::getList();

        $params = $this->getRequest()->requestVar('q'); // use this to access search parameters

        if( isset($params['ParentID']) && $params['ParentID']) {
            $list = $list->filter(array('Categories.ID'=>$params['ParentID'] ));
        }

        return $list;        
    }

}
