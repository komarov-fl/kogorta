<?php

class ExSiteConfig extends DataExtension {

    private static $db = array(
        'Phone' => 'Varchar(255)',
        'Address' => 'Varchar(255)',
        'WorkTime' => 'Varchar(255)',
        'Map' => 'Text',
        'FooterText' => 'Text',
        'Email' => 'Text',
    );
    private static $has_one = array(
        'NoImage' => 'Image',        
        'NoPrice' => 'File'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab("Root.Main", 
            array(
                new TextField('Email', 'Email для сообщений'),
                new TextField('Phone', 'Телефон'),
                new TextField('Address', 'Адрес'),
                new TextField('WorkTime', 'Время работы'),            
                new TextareaField('FooterText', 'Текст в футере'),
                new TextareaField('Map', 'Код карты'),
                new UploadField('NoImage', 'Плейсхолдер изображения'),
                new UploadField('NoPrice', 'Прайс у товара по умолчанию'),
            ),
            "Theme"
        );
        
        
        
    }
}