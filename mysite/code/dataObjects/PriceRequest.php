<?php

class PriceRequest extends DataObject {

    private static $singular_name = 'Запрос стоимости';
    private static $plural_name = 'Запросы стоимости';
    static $default_sort = 'Created DESC';
    private static $db = array(
        'Name' => 'Varchar(255)',
        'Email' => 'Varchar(255)',
        'Comment' => 'Text'
    );
    private static $field_labels = array(
        'Name' => 'Имя',
        'Email' => 'Email',
        'Comment' => 'Комментарий',
        'Product' => 'Товар'
    );
    private static $has_one = array(
        'Product' => 'Product',
    );

}
