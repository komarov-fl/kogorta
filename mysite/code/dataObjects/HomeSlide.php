<?php

class HomeSlide extends DataObject {

    private static $singular_name = 'Слайд';
    private static $plural_name = 'Слайды';
    static $default_sort = 'SortOrder ASC';
    private static $db = array(
        'Title' => 'Varchar(255)',
        'Text' => 'Text',
        'ButtonText' => 'Varchar(255)',
        'Href' => 'Varchar(255)',
        'SortOrder' => 'Int'
    );
    private static $field_labels = array(
        'Title' => 'Заголовок',
        'Text' => 'Текст',
        'ButtonText' => 'Текст на кнопке',
        'Href' => 'Ссылка',
        'Slide' => 'Изображение',
        'SortOrder' => 'Порядок сортировки'
    );
    private static $has_one = array(
        'Slide' => 'Image',
        'Page' => 'HomePage'
    );

    public function getThumb() {
        if ($this->SlideID) {
            return $this->Slide()->ScaleWidth(125)->CropHeight(50);
        }
    }

    public function getCMSFields() {
        
        $slideField = new UploadField('Slide', $this->fieldLabel('Slide'));
        $slideField->setFolderName('Uploads/HomeSlide');
        $fields = new FieldList(
            $slideField,
            new NumericField('SortOrder', $this->fieldLabel('SortOrder')),
            new TextField('Title', $this->fieldLabel('Title')),
            new TextareaField('Text', $this->fieldLabel('Text')),
            new TextField('ButtonText', $this->fieldLabel('ButtonText')),
            new TextField('Href', $this->fieldLabel('Href'))            
        );
        return $fields;
    }

}
