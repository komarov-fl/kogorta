<?php

class Gallery extends DataObject {

     private static $singular_name = 'Галерея';
    private static $plural_name = 'Галерея';
    static $default_sort = 'SortOrder ASC';
    private static $db = array(
        'SortOrder' => 'Int'
    );
    private static $field_labels = array(
        'Image' => 'Изображение',
        'SortOrder' => 'Порядок сортировки'
    );
    private static $has_one = array(
        'Image' => 'Image',
        'Page' => 'Page'
    );

    public function getThumb() {
        if ($this->ImageID) {
            return $this->Image()->ScaleWidth(50)->CropHeight(50);
        }
    }

    public function getCMSFields() {
        
        $slideField = new UploadField('Image', $this->fieldLabel('Image'));
        $slideField->setFolderName('Uploads/Gallery');
        $fields = new FieldList(
            $slideField,
            new NumericField('SortOrder', $this->fieldLabel('SortOrder'))           
        );
        return $fields;
    }

}
