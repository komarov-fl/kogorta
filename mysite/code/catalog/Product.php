<?php

class Product extends DataObject {

    private static $singular_name = 'Товар';
    private static $plural_name = 'Товары';
    static $default_sort = 'ParentID ASC, SortOrder ASC';
    private static $db = array(
        'Name' => 'Varchar(255)',
        "URLSegment" => "Varchar(255)",
        'SEOH1' => 'Varchar(255)',
        'SEOTitle' => 'Varchar(255)',
        'SEODescription' => 'Text',
        'SEOKeywords' => 'Text',        
        'Description' => 'HTMLText',
        'SortOrder' => 'Int',
        'Status' => 'Boolean',
        'ProductCode' => 'Varchar(255)',
        'UseArea' => 'Varchar(255)',
        'Date' => 'Date' 
    );
    private static $indexes = array(
        "URLSegment" => true,
    );
    private static $searchable_fields = array(
        'Name',
        'ProductCode'
    );
    private static $field_labels = array(
        'Name' => 'Наименование',
        "URLSegment" => "URL",
        'SEOH1' => 'SEO H1',
        'SEOTitle' => 'SEO Title',
        'SEODescription' => 'SEO Description',
        'SEOKeywords' => 'SEO Keywords',
        'Description' => 'Описание',
        'Status' => 'Статус',
        'ProductCode' => 'Обозначение',
        'UseArea' => 'Область применения',
        'Image' => 'Изображение',
        'Date' => 'Дата поступления',        
        'SortOrder' => 'Порядок сортировки',
        'Categories' => 'Показывать в категориях',
        'Parent' => 'Родительская категория',
        'Price' => 'Прайс'
    );
    private static $has_one = array(
        'Parent' => 'Category',
        'Image' => 'Image',
        'Price' => 'File'
    );    
    private static $has_many = array(
        'PriceRequests' => 'PriceRequest',
    );
    private static $belongs_many_many = array(
        'Categories' => 'Category'
    );
    
    public function CMSThumbnail() {
        if($this->ImageID) {
            return $this->Image()->Pad(50,50);
        }
        return false;
    }
    
    public function canView($member = null) {
        return true;
    }

    
    public function Link($action = null) {
        return Controller::join_links(Director::baseURL(), $this->RelativeLink($action));
    }
    
    public function AbsoluteLink($action = null) {        
        return Director::absoluteURL($this->Link($action));        
    }

    public function getCMSFields() {

        $baseLink = Controller::join_links(
            Director::absoluteBaseURL(), 
            $this->ParentID ? $this->Parent()->RelativeLink(true) : null
        );
        $urlsegment = SiteTreeURLSegmentField::create("URLSegment", $this->fieldLabel('URLSegment'))
                ->setURLPrefix($baseLink)
                ->setDefaultURL("product-".$this->ID);
        
        $fields = new FieldList(
           $rootTab = new TabSet("Root", 
                    new Tab('Main', "Общие", 
                            new TextField("Name", $this->fieldLabel('Name')), 
                            $urlsegment,
                            new TextField("ProductCode", $this->fieldLabel('ProductCode')),
                            new TextField("UseArea", $this->fieldLabel('UseArea')),
                            new HtmlEditorField('Description', $this->fieldLabel('Description')),
                            new NumericField('SortOrder', $this->fieldLabel('SortOrder'))
                    ),
                    new Tab('SEO', "SEO",
                            new TextField('SEOH1', 'SEO H1'),
                            new TextField('SEOTitle', 'SEO Title'),
                            new TextareaField('SEODescription', 'SEO Description'),
                            new TextareaField('SEOKeywords', 'SEO Keywords')
                    ),
                    new Tab('Relations', "Связи",
                            $ParentField = new DropdownField('ParentID', $this->fieldLabel('Parent'), Category::get()->map('ID', 'NestedName')),
                            $Image = new UploadField('Image', $this->fieldLabel('Image')),
                            $Price = new UploadField('Price', $this->fieldLabel('Price')),
                            $CheckboxSet = new CheckboxSetField('Categories', $this->fieldLabel('Categories'), Category::get()->map('ID', 'NestedName'))
                    )
            )
        );
        
        $ParentField->setEmptyString('Нет');
        $Image->setFolderName('Uploads/Product');
        $Price->setFolderName('Uploads/Product/Price');
        
        $CheckboxSet->setDescription('<style>.optionset li {width: 100%;}</style>');

        return $fields;
    }
    
    public function RelativeLink($action = null) {
        if ($this->ParentID) {
            $parent = $this->Parent();

            $base = $parent->RelativeLink($this->URLSegment);
        } elseif (!$action && $this->URLSegment == RootURLController::get_homepage_link()) {
            // Unset base for root-level homepages.
            // Note: Homepages with action parameters (or $action === true)
            // need to retain their URLSegment.
            $base = null;
        } else {
            $base = $this->URLSegment;
        }
        if($action === true) $action = null;
        return Controller::join_links($base, '/', $action);
    }
    
    protected function onBeforeWrite() {
        parent::onBeforeWrite();
        $count = 2;
        while(!$this->validURLSegment()) {
                $this->URLSegment = preg_replace('/-[0-9]+$/', null, $this->URLSegment) . '-' . $count;
                $count++;
        }
    }
    
    protected function validURLSegment() {
        $product = Product::get()->filter(array(
            'URLSegment' => $this->URLSegment,
            'ID:not' => $this->ID
        ))->first();
        
        $category = Category::get()->filter(array(
            'URLSegment' => $this->URLSegment
        ))->first();
        
        return !($product || $category);
        
    }
    
    public function getBreadcrumbItems($maxDepth = 20, $stopAtPageType = false, $showHidden = false) {
        $page = $this;
        $pages = array();

        while(
            $page
            && (!$maxDepth || count($pages) < $maxDepth)
            && (!$stopAtPageType || $page->ClassName != $stopAtPageType)
        ) {            
            $pages[] = $page;
            
            if($page->ParentID) {
                $page = $page->Parent();
            } else {
                $page = false;
            }
        }

        $items = new ArrayList(array_reverse($pages));
        $home = HomePage::get()->first();
        $items->unshift($home);
        return $items;
    }
    
    public function getMenuTitle() {
        return $this->Name;
    } 
    
    public function getAncestors() {
        $ancestors = new ArrayList();
        $object = $this;
        while ($object->ParentID && $object = $object->Parent()) {
            $ancestors->push($object);
        }
        return $ancestors;
    }
    
    public function getParent() {
        if ($this->ParentID) {
            return $this->Parent();
        }        
        return false;
    }
    
    public function getPrev() {
        return Product::get()
            ->filter(array(
                'ParentId' => $this->ParentID
            ))
            ->filterAny(array(
                'SortOrder:LessThan' => $this->SortOrder,
                'ID:LessThan' => $this->ID
            ))
            ->first();
    }
    
    public function getNext() {
        return Product::get()
            ->filter(array(
                'ParentId' => $this->ParentID
            ))
            ->filterAny(array(
                'SortOrder:GreaterThan' => $this->SortOrder,
                'ID:GreaterThan' => $this->ID
            ))
            ->first();        
    }

}


class Product_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'priceRequest'
    );

    public function init() {
        parent::init();
    }

    public function index() {
        return $this->renderWith(array('ProductPage', 'Page'));
    }
    
    public function priceRequest(SS_HTTPRequest $request) {
        
        if(!$request->isAjax()) {
            return;
        }
        
        $config = SiteConfig::current_site_config(); 
        
        $pr = PriceRequest::create();
         
        $pr->Name = $request->postVar('name');
        $pr->Email = $request->postVar('email');
        $pr->Comment = $request->postVar('comment');
        $pr->ProductID = $this->ID;        
        $pr->write();
        
        $subject = 'Запрос стоимости';
        $template = new SSViewer('PriceRequestEmail');
        $body = $template->process(new ArrayData(array(
            'Name' => $pr->Name,
            'Email' => $pr->Email,
            'Comment' => $pr->Comment,
            'Product' => $this
        )));
        $email = new Email($config->Email, $config->Email, $subject, $body);
        $email->send();
        
        return 'Запрос отправлен';
        
    }
    
}
