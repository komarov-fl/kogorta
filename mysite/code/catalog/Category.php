<?php

class Category extends DataObject {

    private static $singular_name = 'Категория';
    private static $plural_name = 'Категории';
    static $default_sort = 'ParentID ASC, SortOrder ASC';
    private static $db = array(
        'Name' => 'Varchar(255)',
        "URLSegment" => "Varchar(255)",
        'SEOH1' => 'Varchar(255)',
        'SEOTitle' => 'Varchar(255)',
        'SEODescription' => 'Text',
        'SEOKeywords' => 'Text',
        'Description' => 'HTMLText',
        'SortOrder' => 'Int',
        'Status' => 'Boolean',
        'ShowHome' => 'Boolean'
    );
    private static $field_labels = array(
        'Name' => 'Название',
        "URLSegment" => "URL",
        'SEOH1' => 'SEO H1',
        'SEOTitle' => 'SEO Title',
        'SEODescription' => 'SEO Description',
        'SEOKeywords' => 'SEO Keywords',
        'Description' => 'Описание',
        'Status' => 'Статус',
        'ShowHome' => 'Показывать на главной',
        'Image' => 'Изображение',
        'SortOrder' => 'Порядок сортировки',
        'Parent' => 'Родительская категория'
    );
    private static $indexes = array(
        'URLSegment' => 'unique("URLSegment, ParentID")'
    );
    private static $defaults = array(
        'SortOrder' => 0,
        'Status' => 1,
        'ShowHome' => 1
    );
    private static $has_many = array(
        'Products' => 'Product',
        'Categories' => 'Category'
    );
    private static $many_many = array(
        'Products_show' => 'Product'
    );
    private static $has_one = array(
        'Image' => 'Image',
        'Parent' => 'Category'
    );
    public $subMenu = false;
    
    public function CMSThumbnail() {
        if($this->ImageID) {
            return $this->Image()->Pad(50,50);
        }
        return false;
    }


    public function getMenuTitle() {
        return $this->Name;
    } 
    
    public function getNestedName() {
        
        $name = $this->Name;
        $page = $this;
        
        while ($page = $page->getParent()) {
            $name = $page->Name.' > '.$name;
        }
        
        return $name;
    } 
    
    public function isCatalog() {
        return true;
    }
    
    public function isCurrent() {
        $curr = Director::get_current_page();
        return $this->ID == $curr->ID && $this->class == $curr->class ;
    }
    
    public function LinkingMode() {
        if ($this->isCurrent()) {
            return 'current';
        }
        
        if ($this->isSection()) {
            return 'current';
        }
        
        return '';
    }
    
    public function isSection() {
        return in_array(Director::get_current_page()->class, array('Category','Product')) && in_array($this->ID, Director::get_current_page()->getAncestors()->column());
    }
    
    public function getAncestors() {
        $ancestors = new ArrayList();
        $object = $this;
        while ($object->ParentID && $object = $object->Parent()) {
            $ancestors->push($object);
        }
        return $ancestors;
    }
    
    public function getParent() {
        if ($this->ParentID) {
            return $this->Parent();
        }        
        return false;
    }

    public function Children() {
        return $this->Categories();
    }

    public function canView($member = null) {
        return true;
    }

    public function Link($action = null) {
        return Controller::join_links(Director::baseURL(), $this->RelativeLink($action));
    }

    public function getCMSFields() {

        $baseLink = Controller::join_links(
            Director::absoluteBaseURL(), 
            $this->ParentID ? $this->Parent()->RelativeLink(true) : null
        );

        $urlsegment = SiteTreeURLSegmentField::create("URLSegment", $this->fieldLabel('URLSegment'))
                ->setURLPrefix($baseLink)
                ->setDefaultURL("product-" . $this->ID);


        $fields = new FieldList(
            $rootTab = new TabSet("Root", 
                    new Tab('Main', "Общие", 
                            new TextField("Name", $this->fieldLabel('Name')), 
                            $urlsegment,
                            new CheckboxField('ShowHome', $this->fieldLabel('ShowHome')),
                            new HtmlEditorField('Description', $this->fieldLabel('Description')),
                            new NumericField('SortOrder', $this->fieldLabel('SortOrder'))
                    ),
                    new Tab('SEO', "SEO",
                            new TextField('SEOH1', 'SEO H1'),
                            new TextField('SEOTitle', 'SEO Title'),
                            new TextareaField('SEODescription', 'SEO Description'),
                            new TextareaField('SEOKeywords', 'SEO Keywords')
                    ),
                    new Tab('Relations', "Связи",
                            $ParentField = new DropdownField('ParentID', $this->fieldLabel('Parent'), Category::get()->map('ID', 'NestedName')),
                            $Image = new UploadField('Image', $this->fieldLabel('Image'))
                            
                    )
            )
        );
 
        
        $ParentField->setEmptyString('Нет');
        $Image->setFolderName('Uploads/Category');

        return $fields;
    }
    
    public function RelativeLink($action = null) {
        if ($this->ParentID) {
            $parent = $this->Parent();

            $base = $parent->RelativeLink($this->URLSegment);
        } elseif (!$action && $this->URLSegment == RootURLController::get_homepage_link()) {
            $base = null;
        } else {
            $base = $this->URLSegment;
        }
        if($action === true) $action = null;
        return Controller::join_links($base, '/', $action);
    }
    
    
    public function getBreadcrumbItems($maxDepth = 20, $stopAtPageType = false, $showHidden = false) {
        $page = $this;
        $pages = array();

        while(
            $page
            && (!$maxDepth || count($pages) < $maxDepth)
            && (!$stopAtPageType || $page->ClassName != $stopAtPageType)
        ) {            
            $pages[] = $page;
            
            if($page->ParentID) {
                $page = $page->Parent();
            } else {
                $page = false;
            }
        }

        $items = new ArrayList(array_reverse($pages));
        $home = HomePage::get()->first();
        $items->unshift($home);
        return $items;
    }

    protected function onBeforeWrite() {
        parent::onBeforeWrite();
        $count = 2;
        while (!$this->validURLSegment()) {
            $this->URLSegment = preg_replace('/-[0-9]+$/', null, $this->URLSegment) . '-' . $count;
            $count++;
        }
    }

    protected function validURLSegment() {
        $product = Product::get()->filter(array(
                    'URLSegment' => $this->URLSegment
                ))->first();

        $category = Category::get()->filter(array(
                    'URLSegment' => $this->URLSegment,
                    'ID:not' => $this->ID
                ))->first();

        return !($product || $category);
    }
    

}

class Category_Controller extends Page_Controller {    
    
    private $products_limit = 10;

    private static $allowed_actions = array(
        'loadpage'
    );

    public function init() {
        parent::init();
    }

    public function index() {
        return $this->renderWith(array('CategoryPage', 'Page'));
    }
    
    public function loadpage(SS_HTTPRequest $request) {
        
        $page = $request->getVar('page');
        
        if (!$page) {
            $page = 0;
        }
        
        $offset = $this->products_limit * $page;
        
        $products = $this->getProductsShow($offset);
        
        $result = '';
        
         
        if(!$products->exists()) {
            return 0;
        }
        
        foreach ($products as $product) {
            $template = new SSViewer('Product');
            $result .= $template->process(new ArrayData(array(
                'Name' => $product->Name,
                'Image' => $product->Image(),
                'Link' => $product->Link(),
                'ProductCode' => $product->ProductCode
            )));
        }
        
        return $result;
    }

    public function handleRequest(\SS_HTTPRequest $request, \DataModel $model = null) {
        $child = null;
        $action = $request->param('Action');
        $this->setDataModel($model);
        
        if ($action && SiteTree::config()->nested_urls && !$this->hasAction($action)) {
            if (class_exists('Translatable'))
                Translatable::disable_locale_filter();

            $child = $this->model->Category->filter(array(
                        'ParentID' => $this->ID,
                        'URLSegment' => rawurlencode($action)
                    ))->first();

            if (!$child) {
                $child = $this->model->Product->filter(array(
                            'ParentID' => $this->ID,
                            'URLSegment' => rawurlencode($action)
                        ))->first();
            } else if(!$this->subMenu) {
                $child->subMenu = $child->ID;
            } else {
                $child->subMenu = $this->subMenu;
            }

            if (class_exists('Translatable'))
                Translatable::enable_locale_filter();
        }

        if ($child) {
            $request->shiftAllParams();
            $request->shift();
            return ProductCatalogController::controller_for_category($child)->handleRequest($request, $model);
        }
        
        return parent::handleRequest($request, $model);
    }
    
    public function getProductsShow() { 
        $list = $this->Products_show();
        
        $paginated = new PaginatedList($list, $this->getRequest());
        
        $paginated->setLimitItems($this->products_limit);

        return $paginated;
    }
    
    public function getSubMenuCategory() { 
        if(!$this->subMenu) {
            return false;
        }
        
        return Category::get()->byID($this->subMenu);
    }
    
}
