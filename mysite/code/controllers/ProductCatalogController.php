<?php

class ProductCatalogController extends ModelAsController {

    public static function controller_for_category($sitetree, $action = null) {
        if ($sitetree->class == 'SiteTree') {
            $controller = "ContentController";
        } else {
            $ancestry = ClassInfo::ancestry($sitetree->class);
            while ($class = array_pop($ancestry)) {
                if (class_exists($class . "_Controller"))
                    break;
            }
            $controller = ($class !== null) ? "{$class}_Controller" : "ContentController";
        }

        if ($action && class_exists($controller . '_' . ucfirst($action))) {
            $controller = $controller . '_' . ucfirst($action);
        }

        return class_exists($controller) ? Injector::inst()->create($controller, $sitetree) : $sitetree;
    }

    public function getNestedController() {
        $request = $this->getRequest();
        
//        $url = $request->getURL();
//        
//        $cachekey = str_replace(array('/','-'), array('_','__'), trim($url, '/'));
//        
//        $cache = SS_Cache::factory('Category');
//        
//        if($html = $cache->load($cachekey)) {
//            echo $html;
//            die();
//        }
        
        

        if (!$URLSegment = $request->param('URLSegment')) {
            throw new Exception('ModelAsController->getNestedController(): was not passed a URLSegment value.');
        }

        if (class_exists('Translatable'))
            Translatable::disable_locale_filter();


        $conditions = array('"URLSegment"' => rawurlencode($URLSegment));
        if (SiteTree::config()->nested_urls) {
            $conditions[] = array('"ParentID"' => 0);
        }
        $sitetree = DataObject::get_one('Category', $conditions);


        if (class_exists('Translatable'))
            Translatable::enable_locale_filter();

        if (!$sitetree) {
            return parent::getNestedController();
        }

        if (isset($_REQUEST['debug'])) {
            Debug::message("Using record #$sitetree->ID of type $sitetree->class with link {$sitetree->Link()}");
        }

        return self::controller_for_category($sitetree, $this->getRequest()->param('Action'));
    }

}
