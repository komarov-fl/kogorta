<?php

class NewsHolderPage extends Page {

    private static $singular_name = 'Все новости';
    private static $plural_name = 'Все новости';
    private static $db = array(
    );
    
    private static $allowed_children = array(
        'NewsPage'
    );
    
    public function getNews() {
        return NewsPage::get()->filter(array('ParentID' => $this->ID));
    }

}

class NewsHolderPage_Controller extends Page_Controller {
    
    private $newsLimit = 10;

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
    }
    
    public function getPaginatedNews() {
        $paginated = new PaginatedList($this->data()->getNews(), $this->getRequest());        
        $paginated->setLimitItems($this->newsLimit);
        return $paginated;
    }

}
