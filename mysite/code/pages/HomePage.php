<?php

class HomePage extends Page {

    private static $singular_name = 'Главная';
    private static $plural_name = 'Главные';
    private static $db = array(
    );
    private static $has_many = array(
        'Slides' => 'HomeSlide'
    );
    
    private static $allowed_children = array();

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $SlidesConfig = GridFieldConfig_RelationEditor::create();
        $SlidesConfig->getComponentByType('GridFieldDataColumns')->setDisplayFields(array(
            'Thumb' => 'Изображение',
            'SortOrder' => 'Порядок сортировки'
        ));
        $SlidesConfig->removeComponentsByType('GridFieldAddExistingAutocompleter');
        $SlidesConfig->removeComponentsByType('GridFieldDeleteAction');
        $SlidesConfig->addComponent(new GridFieldDeleteAction);
        
        $SlidesField = new GridField(
            'Slides', 
            'Слайдер', 
            $this->Slides(), 
            $SlidesConfig
        ); 

        $sliderTab = new Tab('Slider', "Слайдер", $SlidesField);
        $fields->addFieldToTab('Root', $sliderTab);
        $fields->removeByName('SEOH1');
        return $fields;
    }

}

class HomePage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'feedBack'
    );
    
    public function getSpecial() {
        return SpecialHolderPage::get()->first();
    }
    
    public function getAllNews() {
        return NewsHolderPage::get()->first();
    }
    
    public function getCategories() {
        return Category::get()->filter(array(
            'ParentId' => 0,
            'ShowHome' => 1
        ));
    }

    public function init() {
        parent::init();        
        Requirements::javascript(SSViewer::get_theme_folder().'/javascript/jquery.flexslider-min.js');
    }
    
    public function feedBack(SS_HTTPRequest $request) {
        
        if(!$request->isAjax()) {
            return;
        }
        
        $config = SiteConfig::current_site_config();         
        
        $subject = 'Сообщение с сайта '.$config->Title;
        $template = new SSViewer('PriceRequestEmail');
        $body = $template->process(new ArrayData(array(
            'Name' => $request->postVar('name'),
            'Email' => $request->postVar('email'),
            'Comment' => $request->postVar('comment')
        )));
        $email = new Email($config->Email, $config->Email, $subject, $body);
        $email->send();
        
        return 'Сообщение отправлено';
    }

}
