<?php

class CatalogPage extends Page {

    private static $singular_name = 'Каталог продукции';
    private static $plural_name = 'Каталог продукции';
    private static $db = array(
    );
    
    private static $allowed_children = array(
    );
    
    public function LinkingMode() {
        if(in_array(Director::get_current_page()->class, array('Product', 'Category'))) {
            return 'section';
        }
        return parent::LinkingMode();
    }
    
}

class CatalogPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );
    
    public function getCategories() {
        return Category::get()->filter(array(
            'ParentId' => 0
        ));
    }

    public function init() {
        parent::init();
    }

}
