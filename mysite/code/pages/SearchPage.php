<?php

class SearchPage extends Page {

    private static $singular_name = 'Поиск';
    private static $plural_name = 'Поиск';
    private static $db = array(
    );
    
    private static $allowed_children = array(
    );

}

class SearchPage_Controller extends Page_Controller {
    
    private $products_limit = 10;

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
    }
    
    public function index(SS_HTTPRequest $request) {
        
        $search = $request->getVar('q');
        
        if($search) {
            
            $this->Search = $search;
            
            $Products = Product::get()->filterAny(array(
                'ProductCode:PartialMatch' => $search,
                'Name:PartialMatch' => $search,
            ));
            $paginated = new PaginatedList($Products, $this->getRequest());
        
            $paginated->setLimitItems($this->products_limit);

            $this->Products = $paginated;
        }
        
        return $this->getViewer('index')->process($this);
    }

}
