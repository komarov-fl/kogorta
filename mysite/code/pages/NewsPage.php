<?php

class NewsPage extends Page {

    private static $singular_name = 'Новость';
    private static $plural_name = 'Новости';
    static $default_sort = 'Date DESC';
    private static $db = array(
        'Date' => 'Date',
        'Preview' => 'HTMLText',
    );
    private static $can_be_root = false;
    private static $allowed_children = array(
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        
        $dateField = new DateField('Date', 'Дата');
        $dateField->setConfig('showcalendar', 'true');
        
        $fields->addFieldToTab('Root.Main',
                $dateField,
                'Content'
        );
        
        $Preview = new HtmlEditorField('Preview', 'Превью');
        $Preview->setRows(5);
        $fields->addFieldToTab('Root.Main',
                $Preview,
                'Content'
        );
        
        return $fields;
    }

}

class NewsPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
    }

}
