<?php

class SpecialPage extends Page {

    private static $singular_name = 'Спецпредложение';
    private static $plural_name = 'Спецпредложения';
    private static $db = array(
        'Date' => 'Date',
        'Preview' => 'HTMLText',
    );
    private static $has_one = array(
        'Image' => 'Image'
    );
    private static $can_be_root = false;
    private static $allowed_children = array(
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        
        $dateField = new DateField('Date', 'Дата');
        $dateField->setConfig('showcalendar', 'true');
        
        $fields->addFieldToTab('Root.Main',
                $dateField,
                'Content'
        );
        
        $Preview = new HtmlEditorField('Preview', 'Превью');
        $Preview->setRows(5);
        $fields->addFieldToTab('Root.Main',
                $Preview,
                'Content'
        );
        
        $fields->addFieldToTab('Root.Main',
                $Image = new UploadField('Image','Изображение')
        );
        $Image->setFolderName('Uploads/Special');
        return $fields;
    }

}

class SpecialPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
    }

}
