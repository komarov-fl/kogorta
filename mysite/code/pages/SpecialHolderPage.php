<?php

class SpecialHolderPage extends Page {

    private static $singular_name = 'Спецпредложения';
    private static $plural_name = 'Спецпредложения';
    private static $db = array(
        'HomeTitle' => 'Varchar(255)',
        'HomeText' => 'Text',
        'HomeButtonText' => 'Varchar(255)',
        'HomeHref' => 'Varchar(255)'
    );
    private static $has_one = array(
        'Image' => 'Image'
    );
    private static $field_labels = array(
        'HomeTitle' => 'Заголовок',
        'HomeText' => 'Текст',
        'HomeButtonText' => 'Текст на кнопке',
        'HomeHref' => 'Ссылка',
        'Image' => 'Изображение'
    );
    private static $allowed_children = array(
        'SpecialPage'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();        
        $imageFiled = new UploadField('Image', $this->fieldLabel('Image'));
        $imageFiled->setFolderName('Uploads/Special');
        $homeTab = new Tab("Home","На главной",
            $imageFiled,
            new TextField('HomeTitle', $this->fieldLabel('HomeTitle')),
            new TextareaField('HomeText', $this->fieldLabel('HomeText')),
            new TextField('HomeButtonText', $this->fieldLabel('HomeButtonText')),
            new TextField('HomeHref', $this->fieldLabel('HomeHref'))  
        );        
        $fields->addFieldToTab('Root', $homeTab);
        
        return $fields;
    }

}

class SpecialHolderPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
    }

}
