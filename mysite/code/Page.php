<?php

class Page extends SiteTree {

    private static $singular_name = 'Страница';
    private static $plural_name = 'Страницы';
    private static $db = array(
        'SEOH1' => 'Varchar(255)',
        'SEOTitle' => 'Varchar(255)',
        'SEODescription' => 'Text',
        'SEOKeywords' => 'Text',
    );
    private static $has_one = array(
    );
    private static $has_many = array (
    	'Gallery' => 'Gallery'
    );
    private static $casting = array(
        'GaleryShortcode' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();        
        $fields->removeByName('Metadata');   
        $seoTab= new Tab('SEO', "SEO",
                new TextField('SEOH1', 'SEO H1'),
                new TextField('SEOTitle', 'SEO Title'),
                new TextareaField('SEODescription', 'SEO Description'),
                new TextareaField('SEOKeywords', 'SEO Keywords')
        );
        
        $SlidesConfig = GridFieldConfig_RelationEditor::create();
        $SlidesConfig->getComponentByType('GridFieldDataColumns')->setDisplayFields(array(
            'Thumb' => 'Изображение',
            'SortOrder' => 'Порядок сортировки'
        ));
        $SlidesConfig->removeComponentsByType('GridFieldAddExistingAutocompleter');
        $SlidesConfig->removeComponentsByType('GridFieldDeleteAction');
        $SlidesConfig->addComponent(new GridFieldDeleteAction);
        
        $SlidesField = new GridField(
            'Gallery', 
            'Галерея', 
            $this->Gallery(), 
            $SlidesConfig
        ); 
        
        $fields->addFieldToTab('Root', $seoTab);
        $fields->addFieldToTab('Root', new Tab('Gallery', "Галерея",
                $SlidesField
        ));
        
        return $fields;
    }
    
    public function getBreadcrumbItems($maxDepth = 20, $stopAtPageType = false, $showHidden = false) {
        $items = parent::getBreadcrumbItems($maxDepth, $stopAtPageType, $showHidden);
        $home = HomePage::get()->first();
        $items->unshift($home);
        return $items;
    }
    
    public static function GaleryShortcode($arguments, $content = null, $parser = null, $tagName) {        
        $template = new SSViewer('GaleryShortcode');
        return $template->process(new ArrayData(array(
            'Images' => Director::get_current_page()->Gallery(),
        )));
    }
    public static function FeedbackFormShortcode($arguments, $content = null, $parser = null, $tagName) {        
        $template = new SSViewer('FeedbackForm');
        return $template->process(new ArrayData(array()));
    }
    
    public static function MapShortcode($arguments, $content = null, $parser = null, $tagName) {        
        $config = SiteConfig::current_site_config(); 
        return $config->Map;
    }
              
}

class Page_Controller extends ContentController {

    private static $allowed_actions = array(
    );
    
    public function Permalink($id){
        $page = Page::get()->byID($id);
        return $page ? $page->Link() : false;
    }
    
    public function SearchLink() {
        return SearchPage::get()->first()->Link();
    }

    public function Breadcrumbs($maxDepth = 20, $unlinked = false, $stopAtPageType = false, $showHidden = false) {
        if(method_exists($this->data(), 'getBreadcrumbItems')) {
            $breadcrumbs = $this->data()->getBreadcrumbItems($maxDepth, $stopAtPageType, $showHidden);
        } else {
            $breadcrumbs = new ArrayList();
        }        
        return $breadcrumbs;
    }

    public function MetaTags($includeTitle = true) {
        if(method_exists($this->data(), 'MetaTags')) {
            $tags = $this->data()->MetaTags($includeTitle);
        } else {
            $tags = '<meta name="generator" content="SilverStripe - http://silverstripe.org" />'."\n";
            $tags .='<meta http-equiv="Content-type" content="text/html; charset=utf-8" />'."\n";

        }
        if ($this->SEODescription) {
            $tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->SEODescription) . "\" />\n";
        }
        if ($this->SEOKeywords) {
            $tags .= "<meta name=\"keywords\" content=\"" . Convert::raw2att($this->SEOKeywords) . "\" />\n";
        }

        return $tags;
    }

    public function getSitePhone() {
        $phone = SiteConfig::current_site_config()->Phone;
        if (strpos($phone, ')')) {
            $phone = '<span>' . str_replace(')', ')</span>', $phone);
        }
        
        return $phone;
    }
    
    public function getTopCategories() {
        return Category::get()->filter(array(
            'ParentID' => 0
        ));
    }
    
    public function isCatalog() {
        if(method_exists($this->data(), 'isCatalog')) {
            return $this->data()->isCatalog();
        }
        return false;
    }

    public function init() {
        parent::init();
        Requirements::javascript(SSViewer::get_theme_folder() . '/javascript/jquery-3.0.0.min.js');                
        Requirements::javascript(SSViewer::get_theme_folder().'/javascript/lightbox.min.js');
        Requirements::css(SSViewer::get_theme_folder().'/css/lightbox.min.css');

        if (Permission::checkMember(Member::currentUser(), 'CMS_ACCESS_CMSMain')) {
            Requirements::javascript(SSViewer::get_theme_folder() . '/javascript/cms.js');
        }
    }
    

}
