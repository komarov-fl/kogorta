<div class="catalog-page-catalog">
    <div class="container">
        <div class="caption"><div class="icon"></div><h1><h1><% if $SEOH1 %>$SEOH1<% else %>$Title<% end_if %></h1></h1></div>
        <div class="categories">
            <% loop $Categories %>
            <a class="category<% if $Modulus(4) == 0 %> last<% end_if %>" href="{$Link}">
                <div class="img">
                    $Image.Pad(143,116)
                </div>
                <div class="name">
                    {$Name}
                </div>
            </a>  
            <% end_loop %>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="seo-text text-content">
    <div class="container">
        $Content        
    </div>
</div>
