$(function () {
    var bodyOverflow;
    
    $('nav.primary li').each(function(){
        $(this).css('width',$(this).width());
    });

    if ($().flexslider) {
        $('.home-slider .slider-container-inner').flexslider({
            animation: "slide",
            controlNav: false,
            customDirectionNav: $(".home-slider .slider-container-inner .custom-navigation a"),
            start: function (holder) {
                var totalSlides = $(holder).find('.slides li').not('.clone').length;
                $(holder).find('.total-slides').html(totalSlides);
            },
            after: function (holder) {
                var current = $(holder).find('.slides li.flex-active-slide').index();
                $(holder).find('.current-slide').html(current);
            }
        });
    }

    $('.request-price').on('click', function () {
        popupShow('request-price');
    });

    $('.popup > *, .popup .close').on('click', function () {
        $('.popup').fadeOut();
        $('body').css('overflow', bodyOverflow);
    });

    $('.popup > * > *').on('click', function (event) {
        event.stopPropagation();
    });


    function popupShow(id) {
        $('#' + id).fadeIn().css('display', 'table');
        bodyOverflow = $('body').css('overflow');
        $('body').css('overflow', 'hidden');
    }

    $('.search input').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $(this).parents('form').submit();
        }
    });
    
    var onAjax = false;

    $('#request-price form').on('submit', function () {
        var data = $(this).serialize();
        if(onAjax) {
            return false;
        }
        
        onAjax = true;

        $.ajax({
            url: location.pathname + '/priceRequest',
            type: 'POST',
            data: data,
            success: function (data) {
                var message = $('<div>').addClass('message').html(data);
                $('#request-price form').replaceWith(message);
                onAjax = false;
            }
        });

        return false;
    });
    
    $('#feedback form').on('submit', function () {
        var data = $(this).serialize();
        if(onAjax) {
            return false;
        }
        
        onAjax = true;

        $.ajax({
            url: 'home/feedBack',
            type: 'POST',
            data: data,
            success: function (data) {
                var message = $('<div>').addClass('message').html(data);
                $('#feedback form').replaceWith(message);
                onAjax = false;
            }
        });

        return false;
    });
    
    if ($(window).scrollTop() > $(window).height()) {
        $('.arrow-scroll-top').fadeIn();
    } else {
        $('.arrow-scroll-top').fadeOut();
    }
    
    
    $(window).scroll(function () {
        if ($(window).scrollTop() > $(window).height()) {
            $('.arrow-scroll-top').fadeIn();
        } else {
            $('.arrow-scroll-top').fadeOut();
        }
    });

});

function scroll_top_page() {
    var body = $("html, body");
    body.stop().animate({scrollTop:0}, '500', 'swing');
}