$(function () {
    var inProgress = false;
    var startFrom = 1;
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 500 && !inProgress) {
            $.ajax({
                url: location.pathname+'/loadpage',
                data: {"page": startFrom},
                beforeSend: function () {
                    inProgress = true;
                },
                success: function (data) {
                    if (data == 0) {
                        return;
                    }                    
                    $('.product-table tbody').append(data);                    
                    inProgress = false;
                    startFrom++;
                }
            });
        }
    });
});